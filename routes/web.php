<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
/*--------------------------------ADMIN ROUTE------------------------------------------------------------------------*/
Route::group(['prefix' => 'admin','middleware' => ['auth','admin']],function (){
    Route::get('/',function (){
       return view('admin.dashboard');
    })->name('admin');
    Route::resource('student','StudentController')->except('show');
    Route::resource('teacher','TeacherController')->except('show');
    Route::resource('course','CourseController')->except('show');
    Route::resource('class','ClassController')->except('show');


});
/*--------------------------------END ADMIN ROUTE--------------------------------------------------------------------*/
/*--------------------------------STUDENT ROUTE----------------------------------------------------------------------*/

Route::group(['prefix' => 'student','middleware' => ['auth','student']],function (){
    Route::get('/','HomeController@student')->name('student');
});
/*--------------------------------END STUDENT ROUTE------------------------------------------------------------------*/
/*--------------------------------TEACHER ROUTE----------------------------------------------------------------------*/

Route::group(['prefix' => 'teacher','middleware' => ['auth','teacher']],function (){
    Route::get('/',function (){
        return view('home');
    })->name('teacher');
});
/*--------------------------------END TEACHER ROUTE------------------------------------------------------------------*/
