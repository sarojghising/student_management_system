@extends('admin.layouts.admin')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/dataTable.css') }}">
@endsection
@section('content')
    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"><strong><u>Course List</u></strong></div>
                    <div class="ibox-tools">
                        <a href="{{ route('course.create') }}"><button class="btn btn-outline-success"><i class="fa fa-plus"></i> Add Course</button></a>
                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Course Name</th>
                             <th>Teacher Name</th>
                             <th>Student Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                                @foreach($teacher_name as $key => $value)
                                     <tr>
                                         <td>{{ $key+1 }}</td>
                                         <td>{{ $value->CourseName }}</td>
                                         <td>
                                             @php
                                                 $teacherList = $value
                                                 ->teacherInfo
                                                 ->pluck('teacher_id')
                                                 ->unique()
                                                 ->map(function($query){
                                                    return \App\Models\Teacher::find($query)->name;
                                                 });
                                             @endphp

                                                 @foreach($teacherList as $teacher)
                                                     {{ $teacher}}
                                                     @if(!$loop->last)
                                                         ,
                                                     @endif
                                                 @endforeach



                                         </td>
                                         <td>
                                            @php
                                                 $studentList = $value
                                                    ->studentInfo
                                                    ->pluck('student_id')
                                                    ->unique()
                                                    ->map(function($query){
                                                    return \App\Models\Student::find($query)->name;
                                                    });
                                             @endphp
                                                 @foreach($studentList as $student_data)
                                                     {{ $student_data}}
                                                     @if(!$loop->last)
                                                         ,
                                                     @endif
                                                     @endforeach
                                         </td>
                                         <td>{{ $value->description }}</td>
                                         <td>
                                             <a href="{{ route('course.edit',$value->id) }}" class="btn btn-success mb-2" style="border-radius: 50%"><i class="fa fa-pencil"></i></a>
                                             {{ Form::open(['url' => route('course.destroy',$value->id),'class' => 'form','method' => 'delete','onsubmit' =>'return confirm("Are You Sure Want to Delete")'])}}
                                             {{ Form::button('<i class="fa fa-trash"></i>',['class' => 'btn btn-danger','style' => 'border-radius: 50%','type' => 'submit']) }}
                                             {{ Form::close() }}
                                         </td>
                                     </tr>
                                    @endforeach





                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/admin/js/dataTables.js') }}"></script>

    <script>
        $(document).ready( function () {
            $('.table').DataTable();
        } );
    </script>
@endsection