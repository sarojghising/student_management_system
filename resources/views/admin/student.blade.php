@extends('admin.layouts.admin')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/dataTable.css') }}">
@endsection
@section('content')
    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"><strong><u>Student List</u></strong></div>
                    <div class="ibox-tools">
                        <a href="{{ route('student.create') }}"><button class="btn btn-outline-success"><i class="fa fa-plus"></i> Add Student</button></a>
                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Name</th>
                            <th>Thumbnail</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($student_data)
                            @foreach($student_data as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ ucfirst($value->name) }}</td>
                                    <td>
                                         @if($value->image != null && file_exists(public_path().'/uploads/student/Thumb-'.$value->image))
                                            <img src="{{ asset('uploads/student/Thumb-'.$value->image) }}" alt="image" class="img img-fluid img-thumbnail" style="max-width:150px">
                                        @endif
                                    </td>
                                    <td>
                                        {{ $value->address }}
                                    </td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->number }}</td>
                                    <td>{{ ucfirst($value->status) }}</td>
                                    <td>
                                        <a href="{{ route('student.edit',$value->id) }}" class="btn btn-success mb-2" style="border-radius: 50%"><i class="fa fa-pencil"></i></a>
                                        {{ Form::open(['url' => route('student.destroy',$value->id),'class' => 'form','method' => 'delete','onsubmit' =>'return confirm("Are You Sure Want to Delete")']) }}
                                        {{ Form::button('<i class="fa fa-trash"></i>',['class' => 'btn btn-danger','style' => 'border-radius: 50%','type' => 'submit']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                                @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/admin/js/dataTables.js') }}"></script>

    <script>
        $(document).ready( function () {
            $('.table').DataTable();
        } );
    </script>
@endsection
