@extends('admin.layouts.admin')
@section('content')
    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"><strong><u>Student {{ isset($student_info) ? 'Update' : 'Add' }}</u></strong></div>
                </div>
                <div class="ibox-body">
                    @if(isset($student_info))
                        {{ Form::open(['url' => route('student.update',$student_info->id),'class' => 'form', 'id' => 'student_add','files' => true,'method' => 'patch']) }}
                    @else
                        {{ Form::open(['url' => route('student.store'),'class' => 'form', 'id' => 'student_add','files' => true]) }}
                    @endif
                    <div class="form-group row">
                        {{ Form::label('name','Name: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::text('name',@$student_info->name,['class' => 'form-control'.($errors->has('name') ? 'is-invalid' : ''),'id' => 'name' ,'required' => true,'placeholder' => 'Enter Student Name']) }}
                            @error('name')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('address','Address: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::text('address',@$student_info->address,['class' => 'form-control'.($errors->has('address') ? 'is-invalid' : ''),'id' => 'address' ,'required' => true,'placeholder' => 'Enter Student Address']) }}
                            @error('address')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('email','Email: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::email('email',@$student_info->email,['class' => 'form-control'.($errors->has('email') ? 'is-invalid' : ''),'id' => 'email' ,'required' => true,'placeholder' => 'Enter Student Email']) }}
                            @error('email')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('number','Phone Number: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::number('number',@$student_info->number,['class' => 'form-control'.($errors->has('number') ? 'is-invalid' : ''),'id' => 'number' ,'required' => true,'placeholder' => 'Enter Student Phone Number']) }}
                            @error('number')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        {{ Form::label('status','Status: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::select('status',['active' => 'Active','inactive' => 'Inactive'],@$student_info->status,['class' =>'form-control'.($errors->has('status') ? 'is-invalid': ''),'id' => 'status','required' => true]) }}
                            @error('status')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('image','Image: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-4">
                            {{ Form::file('image',['class' => 'form-control-file'.($errors->has('image') ? 'is-invalid' : ''),'id' => 'image' ,'required' =>(isset($student_info) ? false : true),'accept' => 'image/*']) }}
                            @error('image')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                        <div class="col-sm-4">
                            @if(isset($student_info) && $student_info->image != null && file_exists(public_path().'/uploads/student/Thumb-'.$student_info->image))
                                <img src="{{ asset('uploads/student/Thumb-'.$student_info->image) }}" alt="image" class="img img-fluid img-thumbnail" style="max-width: 150px">
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('','',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                          {{ Form::button('<i class="fa fa-trash"></i> Reset',['class' => 'btn btn-danger','type' =>'reset']) }}
                            {{ Form::button('<i class="fa fa-send"></i> Submit',['class' => 'btn btn-success','type' =>'submit']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection