@extends('admin.layouts.admin')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/dataTable.css') }}">
@endsection
@section('content')
    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"><strong><u>Class List</u></strong></div>
                    <div class="ibox-tools">
                        <a href="{{ route('class.create') }}"><button class="btn btn-outline-success"><i class="fa fa-plus"></i> Add class</button></a>
                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Class</th>
                            <th class="text-success">Teacher Name</th>
                            <th class="text-info">Course Name</th>
                            <th class="text-danger">Student Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($class_info as $key => $value)
{{--                            {{ dd($value) }}--}}
                            <tr>
                                <td class="text-info">{{ $key+1 }}</td>
                                <td>{{ $value->class }}</td>
                                <td class="font-weight-bold">
                                    @php
                                        $teacherList = $value->teacherInfo()->get()->map(function ($teacher){
                                            return $teacher->teacher_info->name;
                                        })->unique('id');

                                    @endphp
                                    @foreach($teacherList as $teacher)
                                        {{ ucfirst($teacher) }}
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach


                                </td>
                                <td class="font-weight-bold">
                                    @php
                                        $course = $value->courseInfo()->get()->map(function ($course){
                                            return $course->course_info->CourseName;
                                        })->unique('id');
                                    @endphp

                                    @foreach($course as $courses)
                                        {{ ucfirst($courses) }}
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach



                                </td>
                                <td class="font-weight-bold">
                                    @php
                                        $students = $value->studentInfo()->get()->map(function ($student){
                                            return $student->student_info->name;
                                        })->unique('id');

                                    @endphp


                                    @foreach($students as $student_info)
                                        {{ ucfirst($student_info) }}
                                        @if(!$loop->last)
                                            ,
                                        @endif
                                    @endforeach

                                </td>
                                <td>{{ ucfirst($value->status) }}</td>
                                <td>
                                    <a href="{{ route('class.edit',$value->id) }}" class="btn btn-success mb-2" style="border-radius: 50%"><i class="fa fa-pencil"></i></a>
                                    {{ Form::open(['url' => route('class.destroy',$value->id),'class' => 'form','method' => 'delete','onsubmit' =>'return confirm("Are You Sure Want to Delete")'])}}
                                    {{ Form::button('<i class="fa fa-trash"></i>',['class' => 'btn btn-danger','style' => 'border-radius: 50%','type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach














                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/admin/js/dataTables.js') }}"></script>

    <script>
        $(document).ready( function () {
            $('.table').DataTable();
        } );
    </script>
@endsection