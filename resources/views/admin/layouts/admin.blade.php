<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>{{ config('app.name') }}</title>
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <!-- GLOBAL MAINLY STYLES-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ asset('assets/admin/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />

    <link href="{{ asset('assets/admin/vendor/themify-icons/css/themify-icons.css') }}" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
    <link href="{{ asset('assets/admin/vendor/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="{{ asset('assets/admin/css/main.min.css') }}" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
    @yield('styles')
</head>

<body class="fixed-navbar">
<div class="page-wrapper">

    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')

    <div class="content-wrapper">
    @include('admin.layouts.notification')
    @yield('content')
        <footer class="page-footer">
            <div class="font-13">{{ date('Y') }} © <b>Admin</b> - All rights reserved.</div>
            <a class="px-4" href="http://themeforest.net/item/adminca-responsive-bootstrap-4-3-angular-4-admin-dashboard-template/20912589" target="_blank">Admin</a>
            <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
        </footer>
    </div>
</div>

<!-- BEGIN PAGA BACKDROPS-->
<div class="sidenav-backdrop backdrop"></div>
<div class="preloader-backdrop">
    <div class="page-preloader">Loading</div>
</div>
<!-- END PAGA BACKDROPS-->
<!-- CORE PLUGINS-->
<script src="{{ asset('assets/admin/vendor/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendor/popper.js/dist/umd/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/vendor/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/admin/vendor/metisMenu/dist/metisMenu.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/admin/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>

<!-- CORE SCRIPTS-->
<script src="{{ asset('assets/admin/js/app.min.js') }}" type="text/javascript"></script>

@yield('scripts')
<script>
    setTimeout(function () {
        $('.alert').slideUp();
    },2000);
</script>
</body>
</html>