<!-- START SIDEBAR-->
<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                <img src="{{ asset('assets/admin/img/admin.png') }}" width="45px"  style="background-color: white;"/>
            </div>
            <div class="admin-info">
                <div class="font-strong">{{ request()->user()->name }}</div><small>{{ request()->user()->name }}</small></div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="{{ route('admin') }}"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{route('student.index') }}"><i class="sidebar-item-icon fa fa-users"></i>
                    <span class="nav-label">Student Management</span>
                </a>
            </li>
            <li>
                <a href="{{ route('teacher.index') }}"><i class="sidebar-item-icon fa fa-map-marker"></i>
                    <span class="nav-label">Teacher Management</span>
                </a>
            </li>
            <li>
                <a href="{{ route('course.index') }}"><i class="sidebar-item-icon fa fa-sitemap"></i>
                    <span class="nav-label">Course Management</span>
                </a>
            </li>
            <li>
                <a href="{{ route('class.index') }}"><i class="sidebar-item-icon fa fa-universal-access"></i>
                    <span class="nav-label">Class Management</span>
                </a>
            </li>
            <li>
                <a href="icons.html"><i class="sidebar-item-icon fa fa-fast-forward"></i>
                    <span class="nav-label">Attendance Management</span>
                </a>
            </li>


        </ul>
    </div>
</nav>
<!-- END SIDEBAR-->