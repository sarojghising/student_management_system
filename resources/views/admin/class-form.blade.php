@extends('admin.layouts.admin')
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/select2.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#teacher_id').select2();
            $('#student_id').select2();
            $('#course_id').select2();
        });
    </script>
@endsection
@section('content')
    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"><strong><u>Class Add</u></strong></div>
                </div>
                <div class="ibox-body">
                    @if(isset($class_info))
                        {{ Form::open(['url' => route('class.update',$class_info->id),'class' => 'form','method' => 'patch']) }}
                        @else
                        {{ Form::open(['url' => route('class.store'),'class' => 'form']) }}
                        @endif
                    <div class="form-group row">
                        {{ Form::label('class','Class: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::text('class',@$class_info->class,['class' => 'form-control'.($errors->has('class') ? 'is-invalid' : ''),'id' => 'class' ,'required' => true,'placeholder' => 'Enter Class']) }}
                            @error('class')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>

                            @enderror

                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('teacher_id','Teacher Name: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::select('teacher_id[]',$teacher_name,@$class_info->teacher_id,['class' =>'form-control'.($errors->has('teacher_id') ? 'is-invalid': ''),'id' => 'teacher_id','required' => true, 'multiple'=>true]) }}
                            @error('teacher_id')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('student_id','Student Name ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::select('student_id[]',$student_name,@$class_info->student_id,['class' =>'form-control'.($errors->has('student_id') ? 'is-invalid': ''),'id' => 'student_id','required' => true, 'multiple'=>true]) }}
                            @error('student_id')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('course_id','Course Name ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::select('course_id[]',$course_name,@$class_info->course_id,['class' =>'form-control'.($errors->has('course_id') ? 'is-invalid': ''),'id' => 'course_id','required' => true, 'multiple'=>true]) }}
                            @error('course_id')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('status','Status: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::select('status',['active' => 'Active','inactive' => 'Inactive'],@$class_infp->status,['class' =>'form-control'.($errors->has('status') ? 'is-invalid': ''),'id' => 'status','required' => true]) }}
                            @error('status')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('','',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::button('<i class="fa fa-trash"></i> Reset',['class' => 'btn btn-danger','type' =>'reset']) }}
                            {{ Form::button('<i class="fa fa-send"></i> Submit',['class' => 'btn btn-success','type' =>'submit']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection