@extends('admin.layouts.admin')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/css/dataTable.css') }}">
@endsection
@section('content')
    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"><strong><u>Teacher List</u></strong></div>
                    <div class="ibox-tools">
                        <a href="{{ route('teacher.create') }}"><button class="btn btn-outline-success"><i class="fa fa-plus"></i> Add Teacher</button></a>
                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Teacher_Name</th>
                            <th>Student_Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>status</th>
                            <th>Thumbnail</th>
                            <th>Address</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($teacher_data)
                            @foreach($teacher_data as $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td>
                                        @if($value->student_info)
                                            @foreach($value->student_info as $student_data)
                                                {{ $student_data->students_info->name }}
                                                @if(!$loop->last)
                                                    ,
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->number }}</td>
                                    <td>{{ ucfirst($value->status) }}</td>
                                    <td>
                                        @if($value->image != null && file_exists(public_path().'/uploads/teacher/Thumb-'.$value->image))
                                            <img src="{{ asset('uploads/teacher/Thumb-'.$value->image) }}" alt="image" class="img img-fluid img-thumbnail" style="max-width:150px">
                                        @endif
                                    </td>
                                    <td>
                                        {{ $value->address }}
                                    </td>
                                    <td>
                                        <a href="{{ route('teacher.edit',$value->id) }}" class="btn btn-success mb-2" style="border-radius: 50%"><i class="fa fa-pencil"></i></a>
                                        {{ Form::open(['url' => route('teacher.destroy',$value->id),'class' => 'form','method' => 'delete'])}}
                                        {{ Form::button('<i class="fa fa-trash"></i>',['class' => 'btn btn-danger','style' => 'border-radius: 50%','type' => 'submit']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/admin/js/dataTables.js') }}"></script>

    <script>
        $(document).ready( function () {
            $('.table').DataTable();
        } );
    </script>
@endsection