@extends('admin.layouts.admin')
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/select2.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/admin/js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#teacher_id').select2();
            $('#student_id').select2();
        });
    </script>
@endsection
@section('content')
    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title"><strong><u>Course Add</u></strong></div>
                </div>
                <div class="ibox-body">
                    @if(isset($course_id))

                        {{ Form::open(['url' => route('course.update',$course_id->id),'class' => 'form','method' => 'patch']) }}
                    @else
                        {{ Form::open(['url' => route('course.store'),'class' => 'form']) }}
                    @endif
                    <div class="form-group row">
                        {{ Form::label('CourseName','CourseName: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::text('CourseName',@$course_id->CourseName,['class' => 'form-control'.($errors->has('CourseName') ? 'is-invalid' : ''),'id' => 'CourseName' ,'required' => true,'placeholder' => 'Enter Course Name Name']) }}
                            @error('CourseName')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>

                            @enderror
                        </div>
                    </div>



                    <div class="form-group row">
                        {{ Form::label('teacher_id','Teacher Name: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::select('teacher_id[]',$teacher_name,@$course_id->teacher_id,['class' =>'form-control'.($errors->has('teacher_id') ? 'is-invalid': ''),'id' => 'teacher_id','required' => true, 'multiple'=>true]) }}
                            @error('teacher_id')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('student_id','Student Name ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::select('student_id[]',$student_name,@$course_id->student_id,['class' =>'form-control'.($errors->has('student_id') ? 'is-invalid': ''),'id' => 'student_id','required' => true, 'multiple'=>true]) }}
                            @error('student_id')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('description','Description: ',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::textarea('description',@$course_id->description,['class' => 'form-control'.($errors->has('description') ? 'is-invalid' : ''),'id' => 'description' ,'rows' => 5, 'cols' => 5,'style' => 'resize:none','required' => true]) }}

                            @error('description')
                            <p class="invalid-feedback">
                                {{ $message }}
                            </p>

                            @enderror
                        </div>
                    </div>





                    <div class="form-group row">
                        {{ Form::label('','',['class' => 'col-sm-3'] )}}
                        <div class="col-sm-9">
                            {{ Form::button('<i class="fa fa-trash"></i> Reset',['class' => 'btn btn-danger','type' =>'reset']) }}
                            {{ Form::button('<i class="fa fa-send"></i> Submit',['class' => 'btn btn-success','type' =>'submit']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection