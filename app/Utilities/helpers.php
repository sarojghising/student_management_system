<?php

//function
function uploadImage($file,$dir,$thumb_dimension = null){
    $path = public_path().'/uploads/'.$dir;
    if (!File::exists($path)){
        File::makeDirectory($path,0777,true,true);
    }
    $file_name = ucfirst($dir).'-'.date('Ymdhis').rand(0,999).".".$file->getClientOriginalExtension();
    $success = $file->move($path,$file_name);
    if ($success){
        $file_path = $path.'/'.$file_name;
        if ($thumb_dimension){
            list($width,$height) = explode('x',$thumb_dimension);
            Image::make($file_path)->resize($width,$height,function ($constraint){
                $constraint->aspectRatio();

            })->save($path.'/Thumb-'.$file_name);
        }
        return $file_name;
    }else{
        return null;
    }


}