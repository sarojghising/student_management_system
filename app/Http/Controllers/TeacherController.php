<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Student_Teacher;
use App\Models\Teacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $teacher = null;
    protected $student = null;
    public function __construct(Teacher $teacher,Student $student )
    {
        $this->student = $student;
        $this->teacher = $teacher;
    }

    public function index()
    {

        $this->teacher = $this->teacher->getAllTeachers();
        return view('admin.teacher')->with('teacher_data',$this->teacher);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->student = $this->student->getAllStudent();
        return view('admin.teacher-form')->with('student_info',$this->student);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $rules = $this->teacher->getRules();
        $request->validate($rules);
        $data = $request->all();
        if ($request->has('image')){
            $teacher_image = uploadImage($request->image,'teacher','300x300');
            if ($teacher_image){
                $data['image'] = $teacher_image;
            }
        }
        $data['added_by'] = request()->user()->id;
        $data['student_data'] = implode(',',$request->student_name)[0];
        $this->teacher->fill($data);
        $success = $this->teacher->save();
        if ($success){
            if($request->student_name){
                foreach($request->student_name as $student_id){
                    $temp = array(
                        'student_id' => $student_id,
                        'teacher_id' => $this->teacher->id
                    );
                    $std_teacher = new Student_Teacher();
                    $std_teacher->fill($temp);
                    $std_teacher->save();
                }
            }
            $request->session()->flash('success','Teacher Added Successfully');
        }else{
            $request->session()->flash('error','There was problem while adding teacher');

        }
        return redirect()->route('teacher.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->teacher = $this->teacher->find($id);
        if (!$this->teacher){
            request()->session()->flash('error','teacher id not found');
            return redirect()->route('student.index');
        }
        $this->student = $this->student->getAllStudent();
        return view('admin.teacher-form')->with('teacher_info',$this->teacher)->with('student_info',$this->student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->teacher->getRules('update');
        $request->validate($rules);
        $this->teacher = $this->teacher->find($id);
        if (!$this->teacher){
              request()->session()->flash('error','teacher id not found');
              return redirect()->route('teacher.index');
        }
        $data = $request->all();

        if ($request->has('image')){
            $teacher_image = uploadImage($request->image,'teacher','300x300');
            $data['image'] = $teacher_image;
            //delete old image
            if (!empty($this->teacher->image) && file_exists(public_path().'/uploads/teacher/image'.$this->teacher->image)){
                unlink(public_path().'/uploads/teacher/image'.$this->teacher->image);
                unlink(public_path().'/uploads/teacher/Thumb-'.$this->teacher->image);

            }

        }
        $this->teacher->fill($data);
        $success = $this->teacher->save();
        if ($success){
            $request->session()->flash('success','Teacher Updated Successfully');
        }else{
            $request->session()->flash('error','There was problem while updating teacher data');
        }
        return redirect()->route('teacher.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $this->teacher = $this->teacher->getId($id);

        if (!$this->teacher){
            request()->session()->flash('error','teacher id not found');
            return redirect()->route('teacher.index');
        }
        $image = $this->teacher->image;
        if ($this->teacher) {
            $this->teacher->student_details->map(function($value){
                if(!empty($value->image) && file_exists(public_path('/uploads/student/'.$value->image))){
                    unlink(public_path('/uploads/student/'.$value->image));
                    unlink(public_path('/uploads/student/thumb-'.$value->image));
                }
                return $value->delete();
            });
            if (!empty($image) && file_exists(public_path('/uploads/teacher/'.$image) )){
                unlink(public_path('/uploads/teacher/'.$image));
                unlink(public_path('/uploads/teacher/thumb-'.$image));
//                unlink(public_path().'/uploads/student/'.$image1);
//                unlink(public_path().'/uploads/student/thumb-'.$image1);

            }
            $this->teacher->delete();

            request()->session()->flash('success','Teacher Data Deleted Successfully');
        }else{
            request()->session()->flash('error','There was problem while deleting teacher data');

        }
        return redirect()->route('teacher.index');
    }
}