<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\CourseTeacher;
use App\Models\Student;
use App\Models\Teacher;
use function foo\func;
use Illuminate\Http\Request;
use function PHPSTORM_META\map;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected  $teacher = null;
    protected  $course = null;
    protected  $student = null;
    public function __construct(Course $course,Teacher $teacher,Student $student)
    {
        $this->teacher = $teacher;
        $this->course = $course;
        $this->student = $student;
    }

    public function index()
    {
        $this->course = $this->course->with(["studentInfo","teacherInfo"])->orderBy("created_at")->get();
        //dd($this->course);

        return view('admin.course')
            ->with('teacher_name',$this->course);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->teacher = $this->teacher->teacherName();
        $this->student = $this->student->getAllStudent();
        return view('admin.course-form')
            ->with('student_name',$this->student)
            ->with('teacher_name',$this->teacher);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $rules = $this->course->getRules();
         $request->validate($rules);
         $data = $request->all();
         $data['teacher_id'] = implode(',',$request->teacher_id)[0];
         $data['student_id'] = implode(',',$request->student_id)[0];
         $this->course->fill($data);
         $success = $this->course->save();
         if ($success){
                 foreach ($request->teacher_id as $value) {
                     foreach ($request->student_id as $values) {
                         $temp = array(
                             'student_id' => $values,
                             'course_id' => $this->course->id,
                             'teacher_id' => $value
                         );
                         $course_teacher = new CourseTeacher();
                         $course_teacher->fill($temp);
                         $course_teacher->save();
                     }
                 }
                 $request->session()->flash('success','Course added Successfully');
                 }else{
             $request->session()->flash('error', 'There was problem while adding Course');
               }

                return redirect(route('course.index'));
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->course = $this->course->find($id);
        $this->teacher = $this->teacher->teacherName();
        $this->student = $this->student->getAllStudent();
        //dd($this->course);
        return view('admin.course-form')
            ->with('teacher_name',$this->teacher)
            ->with('student_name',$this->student)
            ->with('course_id',$this->course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->course->getRules();
        $request->validate($rules);
        $this->course = $this->course->findorFail($id);
        if (!$this->course) {
            request()->session()->flash('error', 'teacher id not found');
            return redirect()->route('teacher.index');
        }
        $data = $request->all();
        //dd($data);
        $data['teacher_id'] = implode(',',$request->teacher_id)[0];
        $data['student_id'] = implode(',',$request->student_id)[0];
        $this->course->fill($data);
        $success = $this->course->save();
        if ($success){
            foreach ($request->teacher_id as $value) {
                foreach ($request->student_id as $values) {
                    $temp = array(
                        'student_id' => $values,
                        'course_id' => $this->course->id,
                        'teacher_id' => $value
                    );
                    $course_teacher = new CourseTeacher();
                    $course_teacher->fill($temp);
                    $course_teacher->save();
                }
            }
            $request->session()->flash('success','Course updated Successfully');
        }else{
            $request->session()->flash('error', 'There was problem while updating Course');
        }
        return redirect(route('course.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->course = $this->course->getID($id);
        //dd($this->course);
         if ($this->course){
             $this->course->teacherInfo()->delete();
             $this->course->studentInfo()->delete();
             $this->course->delete();

             \request()->session()->flash('success','Course deleted Successfully');
         }else{
             \request()->session()->flash('error','There was Problem while deleting course');

         }

         return redirect(route('course.index'));

    }
}
