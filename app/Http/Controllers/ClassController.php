<?php

namespace App\Http\Controllers;

use App\Models\ClassPivot;
use App\Models\Course;
use App\Models\SchoolClass;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $student = null;
    protected $course = null;
    protected $teacher = null;
    protected $class = null;

    public function __construct(Student $student, Course $course, Teacher $teacher, SchoolClass $class)
    {
        $this->student = $student;
        $this->course = $course;
        $this->teacher = $teacher;
        $this->class = $class;
    }

    public function index()
    {
        $this->class = $this->class->with(["studentInfo", "teacherInfo", "courseInfo"])->orderBy("id", 'DESC')->get();
        // dd($this->class);
        return view('admin.class')->with('class_info', $this->class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->teacher = $this->teacher->teacherName();
        $this->student = $this->student->getAllStudent();
        $this->course = $this->course->getAllCourse();
        return view('admin.class-form')
            ->with('student_name', $this->student)
            ->with('course_name', $this->course)
            ->with('teacher_name', $this->teacher);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = $this->class->getRules();
        $request->validate($rules);
        $data = $request->all();
        $data['teacher_id'] = implode(',', $request->teacher_id)[0];
        $data['student_id'] = implode(',', $request->student_id)[0];
        $data['course_id'] = implode(',', $request->course_id)[0];
        $this->class->fill($data);
        $success = $this->class->save();
        if ($success) {
            foreach ($request->teacher_id as $teacher) {
                foreach ($request->student_id as $student) {
                    foreach ($request->course_id as $course) {
                        $temp = array(
                            'student_id' => $student,
                            'course_id' => $course,
                            'teacher_id' => $teacher,
                            'class_id' => $this->class->id
                        );
                        $course_teacher = new ClassPivot();
                        $course_teacher->fill($temp);
                        $course_teacher->save();
                    }
                }
            }
            $request->session()->flash('success', 'Class added Successfully');
        } else {
            $request->session()->flash('error', 'There was problem while adding Class');
        }
        return redirect(route('class.index'));
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->class = $this->class->find($id);
        $this->teacher = $this->teacher->teacherName();
        $this->student = $this->student->getAllStudent();
        $this->course = $this->course->getAllCourse();
        //dd($this->class);
        return view('admin.class-form')
            ->with('student_name', $this->student)
            ->with('course_name', $this->course)
            ->with('teacher_name', $this->teacher)
            ->with('class_info',$this->class);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->class->getRules();
        $request->validate($rules);
        $this->class = $this->class->find($id);
        if (!$this->class){
            \request()->session()->flash('error','There was Problem while updating Class');
            return redirect(route('class.index'));
        }
        $data = $request->all();
        $data['teacher_id'] = implode(',', $request->teacher_id)[0];
        $data['student_id'] = implode(',', $request->student_id)[0];
        $data['course_id'] = implode(',', $request->course_id)[0];
        $this->class->fill($data);
        $success = $this->class->save();
        if ($success) {
            foreach ($request->teacher_id as $teacher) {
                foreach ($request->student_id as $student) {
                    foreach ($request->course_id as $course) {
                        $temp = array(
                            'student_id' => $student,
                            'course_id' => $course,
                            'teacher_id' => $teacher,
                            'class_id' => $this->class->id
                        );
                        $course_teacher = new ClassPivot();
                        $course_teacher->fill($temp);
                        $course_teacher->save();
                    }
                }
            }
            $request->session()->flash('success', 'Class updated Successfully');
        } else {
            $request->session()->flash('error', 'There was problem while updating Class');
        }
        return redirect(route('class.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->class = $this->class->getId($id);
        //dd($this->class);
        if ($this->class) {
//            $this->class->course_details()->g;
            $this->class->course_details()->delete();
            $this->class->student_details()->delete();
            $this->class->teacher_details()->delete();
            $this->class->delete();
            \request()->session()->flash('success', 'Class deleted Successfully');
        } else {
            \request()->session()->flash('error', 'There was Problem while deleting Class');
        }
        return redirect(route('class.index'));
    }
}
