<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return redirect()->route($request->user()->role);
    }

    public function student(Request $request){
        if ($request->user()->role != 'student'){
            $request->session()->flash('status','you are not student');
            return redirect()->route($request->user()->role);
        }
        return view('home');
    }
}
