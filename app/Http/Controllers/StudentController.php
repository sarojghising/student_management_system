<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $student = null;

    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    public function index()
    {
        $this->student = $this->student->orderBy('id','DESC')->get();
        return view('admin.student')->with('student_data',$this->student);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.student-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = $this->student->getRules();
         $request->validate($rules);
         $data = $request->all();
         if ($request->has('image')){
             $student_image = uploadImage($request->image,'student','400x400');
             if ($student_image){
                 $data['image'] = $student_image;
             }
         }
         $data['added_by'] = request()->user()->id;
         $this->student->fill($data);
         $success = $this->student->save();
         if ($success){
             $request->session()->flash('success','Student Added Successfully');
         }else{
             $request->session()->flash('error','Sorry! There was Problem While adding Student');
         }
         return redirect()->route('student.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->student = $this->student->find($id);
        if (!$this->student){
            request()->session()->flash('error','id not found');
        }
        //dd($this->student);
        return view('admin.student-form')->with('student_info',$this->student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rule = $this->student->getRules('update');
        $request->validate($rule);
        $data = $request->all();
        $this->student= $this->student->find($id);

        if (!$this->student){
            request()->session()->flash('error','id not found');
        }
        
        if ($request->has('image')){
            $student_image = uploadImage($request->image,'student','400x400');
            if ($student_image){
                $data['image'] = $student_image;

                if (file_exists(public_path().'/uploads/student/image'.$this->student->image)){
                    unlink(public_path().'/uploads/student/image'.$this->student->image);
                    unlink(public_path().'/uploads/student/Thumb-'.$this->student->image);

                }
            }
        }

        $this->student->fill($data);
        $success = $this->student->save();
        if ($success){
            request()->session()->flash('success','Student Updated Successfully');
        }else{
            request()->session()->flash('error','There was problem while updating student');

        }
        return redirect()->route('student.index');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->student = $this->student->find($id);
        if (!$this->student){
            request()->session()->flash('error','student not found');
            return redirect()->route('student.index');
        }
        $image = $this->student->image;
        $success = $this->student->delete();

        if ($success){
            if ($image != null && file_exists(public_path().'/uploads/student/'.$image)){
                unlink(public_path().'/uploads/student/'.$image);
                unlink(public_path().'/uploads/student/Thumb-'.$image);
            }
            request()->session()->flash('success','Banner Deleted Successfully');
        }else{
            request()->session()->flash('error','There was Problem While deleting student');

        }
        return redirect()->route('student.index');
    }
}
