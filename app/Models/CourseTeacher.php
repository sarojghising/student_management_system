<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseTeacher extends Model
{
    protected $fillable = ['teacher_id','course_id','student_id'];

    public function teacher(){
        return $this->hasOne('App\Models\Teacher','id','teacher_id');
    }
    public  function student(){
        return $this->hasOne('App\Models\Student','id','student_id');

    }
}
