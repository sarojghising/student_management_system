<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassPivot extends Model
{
    protected $fillable = ['teacher_id','course_id','student_id','class_id'];
    public function teacher_info(){
        return $this->hasOne(Teacher::class,'id','teacher_id');
    }
    public function student_info(){
        return $this->hasOne(Student::class,'id','student_id');
    }
    public function course_info(){
        return $this->hasOne(Course::class,'id','course_id');
    }

    public function teacher(){
        return $this->belongsTo(Teacher::class);
    }
    public function student(){

        return $this->belongsTo(Student::class);
    }
    public function course(){
        return $this->belongsTo(Course::class);
    }

}
