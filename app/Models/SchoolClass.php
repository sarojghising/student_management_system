<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
     protected $fillable = ['class','teacher_id','course_id','student_id','status'];
     public function getRules(){
         return [
             'class' => 'required|string',
             'teacher_id' => 'required|exists:teachers,id',
             'course_id' => 'required|exists:courses,id',
             'student_id' => 'required|exists:students,id',
             'status' => 'required|in:active,inactive'
         ];
     }
    public function teacherInfo(){
        return $this->hasMany(ClassPivot::class,'teacher_id','teacher_id')->with('teacher_info');
    }

    public function studentInfo(){
        return $this->hasMany(ClassPivot::class,'student_id', 'student_id')->with('student_info');
    }
    public function courseInfo(){
         return $this->hasMany(ClassPivot::class,'course_id','course_id')->with('course_info');
    }

    public function course_details(){
        return $this->hasMany(ClassPivot::class,'course_id','course_id')->with('course');

    }
    public function  student_details(){
        return $this->hasMany(ClassPivot::class,'student_id','student_id')->with('student');

    }
    public function  teacher_details(){
        return $this->hasMany(ClassPivot::class,'teacher_id','teacher_id')->with('teacher');
    }

    public function getId($id){
         return $this->with(['course_details','student_details','teacher_details'])->findOrFail($id);
    }
}
