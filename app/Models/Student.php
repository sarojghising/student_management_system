<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable =['name','image','address','email','number','status','added_by'];
    public function getRules($act = 'add'){
        $rules =  [
           'name' => 'required|string',
            'image' => 'required|image|max:3000',
            'address' =>'required|string',
            'email' => 'required|email',
            'number' => 'required',
            'status' =>'required|in:active,inactive'
        ];
        if ($act != 'add'){
            $rules['image'] = "sometimes|image";
        }
        return $rules;
    }

    public function getAllStudent(){
        return $this->pluck('name','id');
    }





}
