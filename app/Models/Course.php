<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //
     protected $fillable = ['CourseName','teacher_id','description','student_id'];
     public function getRules(){
         return [
             'CourseName' => 'required|string',
             'teacher_id' => 'required|exists:teachers,id',
             'description' => 'required|string',
             'student_id' => 'required|exists:students,id'
         ];
     }
   public function teacherInfo(){
        return $this->hasMany(CourseTeacher::class,'teacher_id','teacher_id');
    }

    public function studentInfo(){
        return $this->hasMany(CourseTeacher::class,'student_id', 'student_id');
    }

    public function teacher(){
         return $this->hasMany('App\Models\CourseTeacher','teacher_id','id')->with('teacher');
    }
    public function student_data(){
        return $this->hasMany('App\Models\CourseTeacher','student_id','id')->with('student');
    }

    public function getID($id){
         return $this->with('teacher','student_data')->findOrFail($id);
    }
    public function getAllCourse(){
         return $this->pluck('CourseName','id');
    }

}
