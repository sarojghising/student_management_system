<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = ['name','email','number','image','status','address','added_by','student_data'];

    public function getRules($act = 'add'){
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email',
            'number' => 'required|numeric',
            'image' => 'required|image|max:3000',
            'status' => 'required|in:active,inactive',
            'student_data' => 'exists:student,id',
            'address' => 'required|string'
        ];
        if ($act != 'add'){
            $rules['image'] = 'sometimes|image';
        }
        return $rules;
    }

    function student_info(){
        return $this->hasMany('App\Models\Student_Teacher','teacher_id','id')->with('students_info');
    }
    function getAllTeachers(){
        return $this->with('student_info')->orderBy('id','DESC')->get();
    }

    function student_details(){
        return $this->hasMany('App\Models\Student_Teacher','student_id','id')->with('students');
    }
    function  getId($id){
        return $this->with('student_details')->findOrFail($id);
        //return $this->with('student_data')->get();
    }
    function teacherName(){
        return $this->pluck('name','id');
    }






}