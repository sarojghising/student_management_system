<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            array(
              'name' => 'Admin',
              'email' => 'admin@gmail.com',
              'password' => Hash::make('admin123'),
              'status' => 'verified',
              'role' => 'admin'
            ),
            array(
                'name' => 'Teacher',
                'email' => 'teacher@gmail.com',
                'password' => Hash::make('teacher123'),
                'status' => 'verified',
                'role' => 'teacher'
            ),
            array(
                'name' => 'student',
                'email' => 'student@gmail.com',
                'password' => Hash::make('student123'),
                'status' => 'verified',
                'role' => 'student'
            ),

        );
        foreach ($users as $user_data){
            $users = User::where('email',$user_data['email'])->first();
            if (!$users){
                $user = new User();
                $user->fill($user_data);
                $user->save();

            }
        }

    }
}
